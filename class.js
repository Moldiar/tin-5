// Aby użyć tego należy stworzyć przycisk który onClick 
// wywoła funkcję addClassName(cn,text), gdzie cn to objekt zawierający 
// parametr który jest stringiem o nazwie className i text który jest
// tekstem który ma zostać dodany do końca linii className, i
// paragraf o id="demo" w którym zostanie zawarty wynik.
//
// Przykładowy html i wykorzystanie:

// <body>
//     <p>When you click "Try it", a function will be called.</p>
//     <p>The function will display a message.</p>
//     <button onclick="addClassName(obj,'visible')">Try it</button>
//     <p id="demo"></p>
//     <script>
//         var obj = {
//             className: 'first bordered'
//         };
//         function addClassName(cn, text) {
//             cn.className = cn.className + " " + text;
//             document.getElementById("demo").innerHTML = cn.className;
//         }
//     </script>
// </body>

var obj = {
            className: 'first bordered'
        };

        function addClassName(cn, text) {
            cn.className = cn.className + " " + text;
            document.getElementById("demo").innerHTML = cn.className;
        }