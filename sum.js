// Aby użyć tego należy stworzyć przycisk który onClick 
// wywoła funkcję suma(x,y), gdzie x i y są parametrami 
// funkcji iparagraf o id="demo" w którym zostanie zawarty wynik.
//
// Przykładowy html i wykorzystanie:

// <body>
//     <p>When you click "Try it", a function will be called.</p>
//     <p>The function will display a message.</p>
//     <button onclick="suma(0,1)">Try it</button>
//     <p id="demo"></p>
//     <script>
//       function suma(x, y) {
//         document.getElementById("demo").innerHTML = x + y;
//       }
//     </script> 
// </body>

function suma(x, y) {
    document.getElementById("demo").innerHTML = x + y;
}