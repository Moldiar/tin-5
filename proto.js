// Aby użyć tego kodu należy stworzyć przycisk który onclick
// uruchomi funkcjęremovePolishLetters(x) gdzie x jest 
// stringiem który zawiera polskie znaki po czym stworyć paragraf
// o id="demo" w którym treść stringa po zamianie zostanie wyświetlona
//
// Przykładowy html i wykorzystanie:

// <body>
//     <p>When you click "Try it", a function will be called.</p>
//     <p>The function will display a message.</p>
//     <button onclick="removePolishLetters(' Zażółć gęślą jaźń ')">Try it</button>
//     <p id="demo"></p>
//     <script>
//         function removePolishLetters(str) {
//     var pz = ['ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź'];
//     var z = ['a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'];
//     for (var x in pz) {
//         str = str.replace(pz[x], z[x]);
//     }
//     document.getElementById("demo").innerHTML = str;
// }
//     </script>
// </body>

function removePolishLetters(str) {
    var pz = ['ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź'];
    var z = ['a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'];
    for (var x in pz) {
        str = str.replace(pz[x], z[x]);
    }
    document.getElementById("demo").innerHTML = str;
}