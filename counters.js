// Aby móc wywołać należy stworzyć dwa przyciski: jeden onclick
// wywołuje funkcję createCounters(z) gdzie z to chciana ilość counterów
// i drugi przycisk showCounter(t) gdzie t to numer countera który jest szukany
// po czym stworzyć paragraf o id="demo" w którym się wyświetli kod.

// <body>
//     <p>When you click "Try it", a function will be called.</p>
//     <p>The function will display a message.</p>
//     <button onclick="createCounters(10)">Try it</button>
//     <button onclick="showCounter(4)">Try it</button>
//     <p id="demo"></p>
//     <script>
//         var counters = {};

// function ret(y) {
//     return y;
// }

// function createCounters(amount) {
//     for (i = 1; i <= amount; i++) {
//         counters[i] = ret(i);
//     }
// }

// function showCounter(x) {
//     document.getElementById("demo").innerHTML = counters[x];
// }
//     </script>
// </body>

var counters = {};

function ret(y) {
    return y;
}

function createCounters(amount) {
    for (i = 1; i <= amount; i++) {
        counters[i] = ret(i);
    }
}

function showCounter(x) {
    document.getElementById("demo").innerHTML = counters[x];
}